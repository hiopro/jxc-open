/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-12-13 17:04:37
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 16:37:52
 */
import Layout from '@/layout'

export default {
  path: '/setup',
  component: Layout,
  redirect: 'noRedirect',
  meta: { title: '设置', icon: 'shezhi' },
  name: 'Setup',
  children: [
    {
      path: 'account_list',
      name: 'AccountList',
      component: () => import('@/views/bsd/account/list'),
      meta: { title: '结算账户', pid: 'setup1', perm: 'account:list' }
    },
    {
      path: 'employee_list',
      component: () => import('@/views/bsd/employee'),
      name: 'EmployeeList',
      meta: { title: '员工', add: true, pid: 'setup1', perm: 'employee:list' }
    },
    {
      path: 'employee_add',
      hidden: true,
      component: () => import('@/views/bsd/employee/save-data.vue'),
      name: 'EmployeeAdd',
      meta: { title: '新增员工', pid: 'setup1', perm: 'employee:adedit' }
    },
    {
      path: 'employee_edit',
      hidden: true,
      props: (route) => ({ id: route.query.id }),
      component: () => import('@/views/bsd/employee/save-data.vue'),
      name: 'EmployeeEdit',
      meta: { title: '编辑员工', pid: 'setup1', perm: 'employee:adedit' }
    },
    {
      path: 'role_list',
      component: () => import('@/views/sys-manager/role'),
      name: 'RoleList',
      meta: { title: '角色权限', pid: 'setup1', perm: 'role:list' }
    },
    {
      path: 'role_permissions',
      props: (route) => ({ rId: route.query.id, roleName: route.query.roleName }),
      component: () => import('@/views/sys-manager/role/role-permission.vue'),
      name: 'RolePermissions',
      hidden: true,
      meta: { title: '编辑角色权限', pid: 'setup1', perm: 'role:adedit' }
    },
    {
      path: 'guide_list',
      component: () => import('@/views/bsd/guide/list.vue'),
      name: 'GuideList',
      meta: { title: '导购员', pid: 'setup1', perm: 'employee:list' }
    },
    {
      path: 'origin_info',
      component: () => import('@/views/sys-manager/origin/index.vue'),
      name: 'OriginInfo',
      meta: { title: '期初信息', pid: 'setup1', perm: 'origin:list' }
    },
    //
    {
      path: 'business_setup',
      name: 'BusinessSetup',
      component: () => import('@/views/setup/business/index.vue'),
      meta: { title: '业务设置', pid: 'setup2', perm: 'busetup:oper' }
    },
    {
      path: 'sys_reset',
      name: 'SysReset',
      component: () => import('@/views/setup/sys-reset/index.vue'),
      meta: { title: '系统重置', pid: 'setup3', perm: 'system:reset' }
    },
    {
      path: 'credit_balance',
      name: 'CreditBalance',
      component: () => import('@/views/setup/balance/index.vue'),
      meta: { title: '结账', pid: 'setup3', perm: 'balance:list' }
    }
  ]
}
