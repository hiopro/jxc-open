/*
 * @Descripttion: 动态路由
 * @version:
 * @Author: cxguo
 * @Date: 2019-09-08 11:11:06
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-30 14:49:11
 */
import Layout from '@/layout'

const data = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/print',
    name: 'Print',
    props: (route) => ({ title: route.query.title, billCat: route.query.billCat, billId: route.query.billId }),
    component: () => import('@/views/print'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  }
]

export default data
