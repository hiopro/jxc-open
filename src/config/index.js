/*
 * @Descripttion: idnex.js
 * @version: v
 * @Author: cxguo
 * @Date: 2019-06-19 10:32:20
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-02 10:19:04
 */
export default {
  /**
   * @description 配置显示在浏览器标签的title
   */
  title: '中集凯通多式联运平台',
  /**
   * @description token在Cookie中存储的天数，默认1天
   * 设置以当前时间后多长时间失效，需要在utils.js->setCookie()重新赋值
   */
  cookieExpires: new Date(new Date().getTime() + 60 * 1000 * 60),
  /**
   * @description 是否使用国际化，默认为false
   *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
   *              用来在菜单中显示文字
   */
  useI18n: false,
  /**
   * @description api请求基础路径
   */
  baseUrl: {
    dev: window.Glod.dev,
    pro: window.Glod.pro
  },
  /**
   * @description 默认打开的首页的路由name值，默认为home
   */
  homeName: 'home',
  /**
   * @description 需要加载的插件
   */
  plugin: {
    'error-store': {
      // 设为false后不会在顶部显示错误日志徽标
      showInHeader: true,
      // 设为true后在开发环境不会收集错误信息，方便开发中排查错误
      developmentOff: true
    },
    'global-vue': {} // 全局组件插件
  },
  appId: '1bdc66b2ebb75b21f51f5ecc63a43f38'
}
