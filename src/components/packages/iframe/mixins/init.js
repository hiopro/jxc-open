/*
 * @Descripttion: 初始化数据
 * @version:
 * @Author: cxguo
 * @Date: 2019-09-20 11:16:16
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-05 16:06:44
 */
import { iframeAreaStr2int, getMaxIndex, getDocArea } from '../../utils/tool'

const DEFAULT_AREA = {
  width: '800px',
  height: 'auto'
}
const defaultPositionY = '80px'

export default {
  props: {
    area: { type: Array, default: () => { return [DEFAULT_AREA.width, DEFAULT_AREA.height] } }, // 面积
    position: { type: Array, default: () => { return null } } // 面积
  },
  data () {
    return {
    }
  },
  methods: {
    /**
     * 初始化数据
     */
    initData () {
      this.initUiData()
      this.initArea()
      this.initPosition()
      setTimeout(() => {
        this.manualMount()
      }, 100)
    },
    initUiData () {
      this.zIndex = getMaxIndex()
      this.currentTitle = this.title
    },
    initArea () {
      this.normalArea = this.area
      this.currentArea = this.area
    },
    /**
     * 初始化位置
     * 1. 如果有props则使用props
     * 2. 没有props则居中显示，距离顶部
     */
    initPosition () {
      if (this.position) {
        this.normalPosition = this.currentPosition = this.position
        return
      }
      const iframeArea = iframeAreaStr2int(this.area)
      const bodyArea = getDocArea()
      this.currentPosition = [
        (bodyArea.width - iframeArea.width) / 2 + 'px',
        defaultPositionY
      ]
      this.normalPosition = this.currentPosition
    }
  }
}
