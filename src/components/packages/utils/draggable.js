/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-02-05 17:20:24
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-05 16:06:13
 */
/**
 * @description 绑定事件 on(element, event, handler)
 */
export const on = (function () {
  if (document.addEventListener) {
    return function (element, event, handler) {
      if (element && event && handler) {
        element.addEventListener(event, handler, false)
      }
    }
  } else {
    return function (element, event, handler) {
      if (element && event && handler) {
        element.attachEvent('on' + event, handler)
      }
    }
  }
})()

export default {
  inserted: (el, binding, vnode) => {
    const vm = vnode.context
    let triggerDom = el.querySelector(binding.value.trigger)
    triggerDom.style.cursor = 'move'
    let bodyDom = el.querySelector(binding.value.body)
    let pageX = 0
    let pageY = 0
    let transformX = 0
    let transformY = 0
    let canMove = false
    const handleMousedown = e => {
      let transform = /\(.*\)/.exec(bodyDom.style.transform)
      if (transform) {
        transform = transform[0].slice(1, transform[0].length - 1)
        let splitxy = transform.split('px, ')
        transformX = parseFloat(splitxy[0])
        transformY = parseFloat(splitxy[1].split('px')[0])
      }
      pageX = e.pageX
      pageY = e.pageY
      canMove = true
    }
    const handleMousemove = e => {
      let xOffset = e.pageX - pageX + transformX
      let yOffset = e.pageY - pageY + transformY
      if (canMove) bodyDom.style.transform = `translate(${xOffset}px, ${yOffset}px)`
    }
    const handleMouseup = e => {
      const className = e.target.className
      let xOffset = e.pageX - pageX + transformX
      let yOffset = e.pageY - pageY + transformY
      if (['drag-title','text'].indexOf(className) !== -1 ) {
        vm.$emit('on-mouse-up',{translateX:xOffset,translateY:yOffset})
      }
      canMove = false
    }
    on(triggerDom, 'mousedown', handleMousedown)
    on(document, 'mousemove', handleMousemove)
    on(document, 'mouseup', handleMouseup)
  },
  update: (el, binding, vnode) => {
    if (!binding.value.recover) return
    let bodyDom = document.querySelector(binding.value.body)
    bodyDom.style.transform = ''
  }
}
