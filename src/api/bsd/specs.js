/*
 * @Descripttion: 规格
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 12:18:14
 */
import axios from '@/utils/request'
const baseUrl = '/specs'

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/save`,
    method: 'post',
    data: params
  })
}

export function delData(params) {
  return axios.request({
    url: `${baseUrl}/del`,
    method: 'post',
    data: params
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listAllData`,
    method: 'post',
    data: params
  })
}

export function getOptions(params) {
  return axios.request({
    url: `${baseUrl}/listAllData`,
    method: 'post',
    data: params
  })
}

