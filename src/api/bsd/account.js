/*
 * @Descripttion: 账户
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 12:48:39
 */
import axios from '@/utils/request'
const baseUrl = '/account'

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/save`,
    method: 'post',
    data: params
  })
}

export function delData(params) {
  return axios.request({
    url: `${baseUrl}/delete`,
    method: 'post',
    data: params
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

/**
 * 模糊查询
 * @param {*} params
 */
export function options(params) {
  return axios.request({
    url: `${baseUrl}/listOptions`,
    method: 'post',
    data: params
  })
}

export function optionsTree(params) {
  return axios.request({
    url: `${baseUrl}/listTreeOptions`,
    method: 'post',
    data: params
  })
}

export function getdataById(params) {
  return axios.request({
    url: `${baseUrl}/getdataById`,
    method: 'post',
    data: params
  })
}

