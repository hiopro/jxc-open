/*
 * @Descripttion: 库存接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 09:46:48
 */
import axios from '@/utils/request'
const baseUrl = '/goodStockWarn'

/**
 * 库存异常商品
 * @param {*} params
 */
export function countStockWarn(params) {
  return axios.request({
    url: `${baseUrl}/countStockWarn`,
    method: 'post',
    data: params
  })
}

export function getWarningGoods(params) {
  return axios.request({
    url: `${baseUrl}/listWarningGoodsPage`,
    method: 'post',
    data: params
  })
}
