/*
 * @Descripttion: 库存接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 14:42:51
 */
import axios from '@/utils/request'
const baseUrl = '/stockQuery'

/**
 * 查询某商品的库存流水
 * @param {*} params
 */
export function getStockLogBySkuId(skuId) {
  return axios.request({
    url: `${baseUrl}/listStockLogs/${skuId}`,
    method: 'post',
    data: skuId
  })
}
