/*
 * @Descripttion: 角色接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 21:26:55
 */
import axios from '@/utils/request'
const baseUrl = '/role'

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/saveData`,
    method: 'post',
    data: params
  })
}

export function delListData(params) {
  return axios.request({
    url: `${baseUrl}/delListData`,
    method: 'post',
    data: params
  })
}

export function updatePermissions(params) {
  return axios.request({
    url: `${baseUrl}/updatePermissions`,
    method: 'post',
    data: params
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function getOptions(params) {
  return axios.request({
    url: `${baseUrl}/listOptions`,
    method: 'post',
    data: params
  })
}

