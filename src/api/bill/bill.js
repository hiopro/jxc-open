/*
 * @Descripttion: 票据接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-05 17:12:39
 */
import axios from '@/utils/request'
const baseUrl = '/bill'
/**
 * 获取单据主表和明细
 * @param {*} params
 */
export function getBillAndDetail(params) {
  return axios.request({
    url: `${baseUrl}/get_bill_and_detail`,
    method: 'post',
    data: params
  })
}

export function getBillAndDetailDefault(params) {
  return axios.request({
    url: `${baseUrl}/get_bill_and_detail_default`,
    method: 'post',
    data: params
  })
}

