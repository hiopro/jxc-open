/*
 * @Descripttion: 票据接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 13:21:51
 */
import axios from '@/utils/request'
const baseUrl = '/bill/purchaseReturn'

/**
 * 添加进货退货单（要进库存）
 * @param {*} params
 */
export function addData(params) {
  return axios.request({
    url: `${baseUrl}/addData`,
    method: 'post',
    data: params
  })
}

export function cancleBill(params) {
  return axios.request({
    url: `${baseUrl}/cancle`,
    method: 'post',
    data: params
  })
}

/**
 * 查询待退货单
 * @param {} params
 */
export function listWaite2ReturnBillsPage(params) {
  return axios.request({
    url: `${baseUrl}/listWaite2ReturnBillsPage`,
    method: 'post',
    data: params
  })
}

/**
 * 查询进货退货列表
 * @param {*} params
 */
export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function getBillAndDetail(params) {
  return axios.request({
    url: `${baseUrl}/getBillAndDetails/${params}`,
    method: 'post',
    data: params
  })
}

/**
 * 生成票据编号
 * @param {*} params
 */
export function getBillcode(params) {
  return axios.request({
    url: `${baseUrl}/getBillNo`,
    method: 'post',
    data: params
  })
}

export function print(params) {
  return axios.request({
    url: `${baseUrl}/print`,
    method: 'post',
    data: params
  })
}
