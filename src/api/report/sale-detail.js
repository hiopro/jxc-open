/*
 * @Descripttion: 客户接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 09:55:27
 */
import axios from '@/utils/request'
const baseUrl = '/report/saleDetail'

export function listSaleDetail(params) {
  return axios.request({
    url: `${baseUrl}/listSaleDetail`,
    method: 'post',
    data: params
  })
}
