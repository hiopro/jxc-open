/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-07-04 01:36:52
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-15 11:15:34
 */
import { login, getUserInfo, initApp } from '@/api/sys/users'
import { register } from '@/api/sys/tenant.js'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'
import { Message } from 'element-ui'

const state = {
  token: getToken(),
  userInfo: null,
  appData: null
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_USER_INFO: (state, data) => {
    state.userInfo = data
  },
  SET_APP_INFO: (state, data) => {
    state.appData = data
  },
  SET_BUS_SETUP: (state, data) => {
    Object.keys(data).forEach(key => {
      state.appData.businessSetup[key] = data[key]
    })
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      login(userInfo).then(response => {
        const { data } = response
        if (!data.flag) {
          Message.error(data.message)
          reject(data)
        }
        const token = data.data
        commit('SET_TOKEN', token)
        setToken(token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  /**
   * 租户注册
   * @param {*} param0
   * @param {*} userInfo
   */
  register({ commit }, params) {
    return new Promise((resolve, reject) => {
      register(params).then(res => {
        const { data } = res
        if (!data.flag) {
          Message.error(data.message)
          reject(data)
        }
        const token = data.data
        commit('SET_TOKEN', token)
        setToken(token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getUserInfo().then(res => {
        const data = res.data.data
        if (!data) {
          reject('Verification failed, please Login again.')
        }
        commit('SET_USER_INFO', data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  setBusSetup({ commit, state }, data) {
    commit('SET_BUS_SETUP', data)
  },

  initApp({ commit, state }) {
    return new Promise((resolve, reject) => {
      initApp().then(res => {
        const data = res.data.data
        if (!data) {
          reject('初始化应用失败！')
        }
        commit('SET_APP_INFO', data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      // 清除token
      commit('SET_TOKEN', null)
      // 清除appData
      commit('SET_APP_INFO', null)
      removeToken()
      resetRouter()
      dispatch('tagsView/delAllCachedViews', {}, { root: true })
      dispatch('tagsView/delAllVisitedViews', {}, { root: true })
      resolve()
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', null)
      removeToken()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

