/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-02-22 20:27:19
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-08 15:03:06
 */
import config from '@/config'

/**
 * 关闭tab
 * @param {route} view
 */
export const closeSelectedTag = function(view) {
  this.$store.dispatch('tagsView/delView', view).then(({ visitedViews }) => {
    if (view.path === this.$route.path) {
      toLastView.bind(this)(visitedViews, view)
    }
  })
  function toLastView(visitedViews, view) {
    const latestView = visitedViews.slice(-1)[0]
    if (latestView) {
      this.$router.push(latestView)
    } else {
      // now the default is to redirect to the home page if there is no tags-view,
      // you can adjust it according to your needs.
      if (view.name === 'Dashboard') {
        // to reload home page
        this.$router.replace({ path: '/redirect' + view.fullPath })
      } else {
        this.$router.push('/')
      }
    }
  }
}

export const closeCurrentTag = function(view) {
  if (view) {
    closeSelectedTag(view)
  } else {
    closeSelectedTag.bind(this)(this.$route)
  }
}

export const getDomain = function() {
  const baseUrl = process.env.NODE_ENV === 'development' ? config.baseUrl.dev : config.baseUrl.pro
  return baseUrl
}
