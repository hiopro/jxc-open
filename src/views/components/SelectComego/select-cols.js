/*
 * @Descripttion: 下拉框的数据
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-18 15:20:50
 */
export default {
  data() {
    return {
      colsCustomer: [
        { field: 'code', title: '客户编号', width: 150 },
        { field: 'name', title: '客户名称', width: 160 },
        { field: 'contacter', title: '联系人', width: 100 },
        { field: 'phone', title: '联系电话', width: 120 }
      ],
      colsSupplier: [
        { field: 'code', title: '供应商编号', width: 150 },
        { field: 'name', title: '供应商名称', width: 160 },
        { field: 'contacter', title: '联系人', width: 100 },
        { field: 'phone', title: '联系电话', width: 120 }
      ]
    }
  },
  methods: {
  }
}

