/*
 * @Descripttion: table列数据
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-02-21 18:24:10
 */

import clonedeep from 'clonedeep'

export default {
  data() {
    return {
      originColumnsData: [
        { field: 'name',
          title: '商品名称/规格',
          positionDisable: true,
          showOverflow: true,
          width: '280'
        },
        { field: 'code',
          title: '商品编号',
          positionDisable: true
        },
        { field: 'quantity', title: '数量',
          positionDisable: true
        },
        { field: 'changeQuantity', title: '已转数量',
          positionDisable: true
        },
        { field: 'unit', title: '单位',
          positionDisable: true,
          slots: {
            default: ({ row, seq }) => {
              const { options, unit } = row
              const unitOptions = options.unit || []
              const resultArr = unitOptions.filter(item => { return item.value === unit })
              let value = ''
              if (resultArr.length !== 0) {
                value = resultArr[0].label || ''
              }
              return [
                <span>{value}</span>
              ]
            }
          }
        },
        { field: 'price', title: '单价', align: 'right' },
        { field: 'total', title: '金额', align: 'right' },
        { field: 'remark', title: '备注' }
      ]
    }
  },
  computed: {
    columnsData: function(params) {
      const changeType = this.dataObj.changeType
      if (changeType === 1) return this.originColumnsData
      const cols = clonedeep(this.originColumnsData)
      if (changeType === 2) {
        const col = { field: 'noTransQuantity', title: '未转数量',
          positionDisable: true,
          slots: {
            default: (params) => {
              // eslint-disable-next-line no-unused-vars
              const h = this.$createElement
              const { row } = params
              const { quantity, changeQuantity } = row
              const value = quantity - changeQuantity
              return [<span>{value}</span>]
            }
          }
        }
        cols.splice(3, 1, col)
      }
      if (changeType === 3) {
        const col = { field: 'billNo', title: '销售单编号',
          positionDisable: true,
          slots: {
            default: (params) => {
              // eslint-disable-next-line no-unused-vars
              const h = this.$createElement
              const { row } = params
              const { billNo } = row
              const _this = this
              const data = {
                props: {
                  type: 'primary'
                },
                on: {
                  click: function(e) {
                    const { billId } = row
                    _this.jump2purchaseView({ billId })
                  }
                }
              }
              return [<el-link {...data}>{billNo}</el-link>]
            }
          }
        }
        cols.splice(3, 1, col)
      }
      return cols
    }
  }
}

