/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:24:45
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-08 13:40:08
 */
import add from './add'
import update from './update'
import columns from './columns'
export { add, update, columns }
