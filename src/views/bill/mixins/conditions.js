/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 14:42:06
 */
import clonedeep from 'clonedeep'

export default {
  data() {
    return {
      originSearchData: {},
      insSearchData: {},
      optionsData: {}
    }
  },
  props: {
    /**
     * 搜索条件
     */
    queryData: {
      type: Object,
      default: () => { return {} }
    }
  },
  created() {
    this.originSearchData = clonedeep(this.queryData)
    this.insSearchData = this.queryData
  },
  methods: {
    clearData() {
      const originSearchData = clonedeep(this.originSearchData)
      const inKeys = Object.keys(this.insSearchData)
      const oKeys = Object.keys(this.originSearchData)
      inKeys.forEach(key => {
        if (oKeys.indexOf(key) === -1) {
          this.$delete(this.insSearchData, key)
        } else {
          this.$set(this.insSearchData, key, originSearchData[key])
        }
      })
      if (this.insSearchData['dateTab'] === this.originSearchData['dateTab']) {
        const dateTab = this.insSearchData['dateTab']
        this.$refs.date.emitDateByCurrentIndex(dateTab)
      }
    },
    onDateChange(data) {
      const { beginTime, endTime } = data
      this.$set(this.insSearchData, 'bBeginTime', beginTime)
      this.$set(this.insSearchData, 'bEndTime', endTime)
    }
  }
}
