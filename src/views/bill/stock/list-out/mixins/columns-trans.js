/*
 * @Descripttion: table列数据
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-22 13:37:41
 */

import clonedeep from 'clonedeep'

export default {
  data() {
    return {
      originColumnsData: [
        { field: 'name',
          title: '商品名称/规格',
          positionDisable: true,
          showOverflow: true,
          width: 280
        },
        { field: 'code',
          title: '商品编号',
          width: 150,
          positionDisable: true
        },
        { field: 'quantity', title: '数量',
          width: 150,
          positionDisable: true
        },
        { field: 'changeQuantity', title: '已入库数量',
          width: 150,
          positionDisable: true
        },
        { field: 'notChangeQuantity', title: '未入库数量',
          width: 150,
          slots: {
            default: ({ row, seq }) => {
              const { quantity, changeQuantity } = row
              const value = Number(quantity) - Number(changeQuantity)
              return [<span>{value}</span>]
            }
          },
          positionDisable: true
        },
        { field: 'unitName', title: '单位',
          width: 150,
          positionDisable: true
        },
        { field: 'remark',
          width: 200,
          title: '备注'
        }
      ]
    }
  },
  computed: {
    columnsData: function(params) {
      const changeType = this.dataObj.changeType
      if (changeType === 1) return this.originColumnsData
      const cols = clonedeep(this.originColumnsData)
      if (changeType === 2) {
        cols.splice(2, 2)
      }
      if (changeType === 3) {
        const col = { field: 'billNo', title: '入库单编号',
          positionDisable: true,
          slots: {
            default: (params) => {
              // eslint-disable-next-line no-unused-vars
              const h = this.$createElement
              const { row } = params
              const { billNo } = row
              const _this = this
              const data = {
                props: {
                  type: 'primary'
                },
                on: {
                  click: function(e) {
                    const { billId } = row
                    _this.jump2purchaseView({ billId })
                  }
                }
              }
              return [<el-link {...data}>{billNo}</el-link>]
            }
          }
        }
        cols.splice(3, 2, col)
      }
      return cols
    }
  }
}

