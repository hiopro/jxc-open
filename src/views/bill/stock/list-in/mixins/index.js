/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:24:45
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-04 07:36:59
 */
import common from './common'
import update from './update'
export { common, update }
