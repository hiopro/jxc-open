
/*
 * @Descripttion: 价格的变化
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-24 07:50:54
 */
import { getMoreButton, getPriceButton, getStockButton } from '@/components/Table/oper-btns.js'
import { getUnitOptionsByUnitStr } from '@/views/goods-select/mixins/utils.js'
import SelectUnit from '@/views/goods-select/SelectUnit'
import XEUtils from 'xe-utils'

export default {
  data() {
    return {
      columnsImmediately: [
        { title: '操作', width: 140,
          slots: {
            default: (params) => {
              const h = this.$createElement
              const moreButtonNode = getMoreButton(h, { props: { type: 'text' },
                on: { click: (row) => { this.btnMore(params) } }})
              const getStockButtonNode = getStockButton(h, { props: { type: 'text' },
                on: { click: (row) => { this.btnGetStock(params) } }})
              const getPriceButtonNode = getPriceButton(h, { props: { type: 'text' },
                on: { click: (row) => { this.btnJump2Price(params) } }})
              const divNode = h('div', {
                attrs: {
                  class: 'table-oper-btns'
                }
              }, [moreButtonNode, getStockButtonNode, getPriceButtonNode])
              return [divNode]
            }
          }
        },
        { field: 'code', title: '商品编号', width: 140 },
        { field: 'name', title: '商品名称', width: 200, showOverflow: true },
        { field: 'unitId', title: '单位', width: 120,
          slots: {
            default: (params) => {
              return this.getUnitCol(params)
            }
          }
        },
        { field: 'canUseQuantity', title: '可用库存', width: 100,
          slots: {
            default: (params) => {
              const { row } = params
              const { unitMulti } = row
              const canUseQuantity = this.getQtyCanuse(row)
              const value = Number(canUseQuantity) / Number(unitMulti)
              return [<span>{value}</span>]
            },
            header: ({ column }) => {
              return [
                <el-tooltip effect='dark' content='可用库存 = 当前存货 + 待入库量 - 待出库量' placement='top-end'>
                  <span>可用库存<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        },
        { field: 'originQuantity', title: '期初库存', width: 100,
          slots: {
            default: ({ row }) => {
              const { originQuantity, unitMulti } = row
              const value = Number(originQuantity || 0) / Number(unitMulti)
              return [<span>{value}</span>]
            },
            header: ({ column }) => {
              return [
                <el-tooltip effect='dark' content='期初库存为使用此软件前的库存' placement='top-end'>
                  <span>期初库存<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        },
        { field: 'quantity', title: '当前存货', width: 100,
          slots: {
            default: (params) => {
              const { row } = params
              const { unitMulti } = row
              const value = this.getQtyInStore(row)
              const result = Number(value) / Number(unitMulti)
              return [<span>{result}</span>]
            },
            header: ({ column }) => {
              return [
                <el-tooltip effect='dark' content='当前存货=在仓库的货品（不含在途库存）' placement='top-end'>
                  <span>当前存货<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        },
        { field: 'waitQuantityIn', title: '待入库量', width: 100 },
        { field: 'waitQuantityOut', title: '待出库量', width: 100 },
        { field: 'costPrice', title: '成本价', width: 100,
          slots: {
            header: ({ column }) => {
              return [
                <el-tooltip effect='dark' content='成本价是根据商品进货加权平均计算而来' placement='top-end'>
                  <span>成本价<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        },
        { field: 'totalPrice', title: '库存总额', width: 150,
          slots: {
            default: (params) => {
              const { row } = params
              const { costPrice } = row
              const canUseQty = this.getQtyCanuse(row)
              const value = this.$amount(costPrice).multiply(canUseQty).format()
              return [<span>{value}</span>]
            },
            header: ({ column }) => {
              return [
                <el-tooltip effect='dark' content='库存总额 = 成本价 X 可用库存' placement='top-end'>
                  <span>库存总额<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        }
      ],
      columnsDate: [
        { title: '操作', width: 80,
          slots: {
            default: (params) => {
              const h = this.$createElement
              const moreButtonNode = getMoreButton(h, { props: { type: 'text' },
                on: { click: (row) => { this.btnMore(params) } }})
              const divNode = h('div', {
                attrs: {
                  class: 'table-oper-btns'
                }
              }, [moreButtonNode])
              return [divNode]
            }
          }
        },
        { field: 'code', title: '商品编号', width: 140 },
        { field: 'name', title: '商品名称', width: 200, showOverflow: true },
        { field: 'unitName', title: '单位', width: 120,
          slots: {
            default: (params) => {
              return this.getUnitCol(params)
            }
          }
        },
        { field: 'canUseQuantity', title: '截止结余', width: 100,
          slots: {
            default: (params) => {
              const { row } = params
              const value = this.getQtyCanuse(row)
              return [<span>{value}</span>]
            },
            header: ({ column }) => {
              return [
                <el-tooltip effect='dark' content='可用库存 = 当前存货 + 待入库量 - 待出库量' placement='top-end'>
                  <span>截止结余<i class='color-bule el-icon-question'/></span>
                </el-tooltip>
              ]
            }
          }
        }
      ]
    }
  },
  methods: {
    getUnitCol({ row, column, seq }) {
      const options = getUnitOptionsByUnitStr(row)
      if (options.length === 1) {
        const unitObj = options[0]
        return [unitObj.name]
      }
      const data = {
        props: { options },
        model: {
          value: XEUtils.get(row, column.property),
          callback(value) {
            XEUtils.set(row, column.property, value)
          }
        },
        on: {
          'change': (data) => {
            const unitData = data
            this.onUnitChange({ unitData, row })
          }
        }
      }
      return [<SelectUnit {...data}/>]
    },
    onUnitChange({ unitData, row }) {
      const { id, name, multi } = unitData
      this.$set(row, 'unitId', id)
      this.$set(row, 'unitName', name)
      this.$set(row, 'unitMulti', multi)
    }
  }
}

