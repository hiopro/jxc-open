/*
 * @Descripttion: 输入商品的方法
 * @version:
 * @Author: cxguo
 * @Date: 2019-10-18 15:37:24
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-13 11:29:37
 */
import { getPriceBySkuAndUnit } from '@/api/goods/goods-sku.js'

export default {
  data() {
    return {
      simpleIframe: null
    }
  },
  methods: {
    onSelectData($table) {
      const { data, $rowIndex } = $table
      const targetData = data
      const rowIndex = $rowIndex
      this.$set(targetData, 'quantity', 1)
      this.$set(targetData, 'price', '0.00')
      this.handleGoodData(targetData, rowIndex)
      const tableData = this.getTableData()
      const row = tableData[rowIndex]
      this.handleTableActiveCell(row)
    },
    handleTableActiveCell(row) {
      this.setTableActiveCell(row, 'quantity')
    },
    handleGoodData(row, rowIndex) {
      let targetData = row
      const { unitIdStr, unitIsbaseStr, skuId } = targetData
      let { discount } = targetData
      if (unitIdStr && unitIdStr !== '') {
        const baseUnitId = this.getBaseUnitId(unitIdStr, unitIsbaseStr)
        targetData.unitId = baseUnitId
        const params = { skuId, unitId: baseUnitId }
        if (this.isSetPrice) {
          getPriceBySkuAndUnit(params).then(res => {
            if (res.data.flag) {
              const { price } = res.data.data
              if (price) {
                const priceStr = price
                const defaultPrice = this.getBasePrice(price)
                if (this.isOpenDiscount) {
                  discount = discount || this.$amount(100).format()
                  this.updateTableData(rowIndex, { priceDef: defaultPrice, discount, priceStr })
                } else {
                  this.updateTableData(rowIndex, { price: defaultPrice, priceStr })
                }
              }
            }
          })
        }
      }
      targetData = this.formatGoodListData([targetData])[0]
      this.updateTableData(rowIndex, targetData)
      this.closeSimpleGoodSelect()
    },
    /**
     * 格式化数据，外部可以重写此方法，过滤数据
     */
    formatGoodListData(daraArr) {
      return daraArr
    },
    getBaseUnitId(unitIdStr, unitIsbaseStr) {
      const unitIsbaseArr = unitIsbaseStr.split(',')
      const unitIdArr = unitIdStr.split(',')
      const index = unitIsbaseArr.findIndex(item => { return item === '1' })
      return unitIdArr[index]
    },
    getBaseUnitObj(unitIdStr, unitIsbaseStr, unitMultiStr) {
      const unitIsbaseArr = unitIsbaseStr.split(',')
      const unitIdArr = unitIdStr.split(',')
      const unitMultiArr = unitMultiStr.split(',')
      const index = unitIsbaseArr.findIndex(item => { return item === '1' })
      return {
        unitId: unitIdArr[index],
        isBase: unitIsbaseArr[index],
        multi: unitMultiArr[index]
      }
    },
    getBasePrice(priceStr) {
      const priceObj = JSON.parse(priceStr)
      const key = this.defaultPriceCat
      const price = priceObj[key]
      return price
    },
    closeSimpleGoodSelect() {
      const iframe = this.simpleIframe
      if (iframe) {
        iframe.$destroy()
        iframe.$el.parentNode.removeChild(iframe.$el)
        this.simpleIframe = null
      }
    }
  }
}

