/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-06-23 15:49:23
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-23 17:20:18
 */
import { amount } from '@/libs/tools.js'

/**
 * 根据row获取sku的单位下拉框
 * @param {*} param0
 */
export const getUnitOptionsByUnitStr = function({ unitIdStr, unitNameStr, unitMultiStr, unitIsbaseStr }) {
  if (!unitIdStr) return []
  const unitIdArr = unitIdStr.split(',')
  const unitNameArr = unitNameStr.split(',')
  const unitMultiArr = unitMultiStr.split(',')
  const unitIsbaseArr = unitIsbaseStr ? unitIsbaseStr.split(',') : []
  const len = unitIdArr.length
  const result = []
  for (let i = 0; i < len; i++) {
    const obj = {
      id: unitIdArr[i],
      name: unitNameArr[i],
      multi: unitMultiArr[i],
      isBase: unitIsbaseArr[i]
    }
    result.push(obj)
  }
  return result
}

export const getBaseUnitByUnitStr = function({ unitIdStr, unitNameStr, unitMultiStr, unitIsbaseStr }) {
  if (!unitIdStr) return {}
  const unitIdArr = unitIdStr.split(',')
  const unitNameArr = unitNameStr.split(',')
  const unitMultiArr = unitMultiStr.split(',')
  const unitIsbaseArr = unitIsbaseStr ? unitIsbaseStr.split(',') : []
  const index = unitIsbaseArr.findIndex(item => { return Number(item) === 1 })
  return {
    unitId: unitIdArr[index],
    unitName: unitNameArr[index],
    unitIsbase: unitIsbaseArr[index],
    unitMulti: unitMultiArr[index]
  }
}

export const getGoodsTotalPriceByRow = function(row) {
  const { quantity, price } = row
  if (!price || !quantity) return '0.00'
  return amount(price).multiply(quantity).format()
}
